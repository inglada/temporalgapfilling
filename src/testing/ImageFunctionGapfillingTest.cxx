/*=========================================================================

  Program:   gapfilling
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See gapfilling-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "otbGapfillingTests.h"
#include <fstream>
#include <tuple>
#include "otbImageFileWriter.h"

using ImageType = otb::VectorImage<ValueType, 2>;
using LinearFunctorType =
  GapFilling::LinearGapFillingFunctor<ImageType::PixelType>;

void create_image(std::string ima_name, PixelType pix)
{
  ImageType::IndexType start = {0,0};
  ImageType::SizeType size = {1,1};
  ImageType::RegionType region;
  region.SetSize(size);
  region.SetIndex(start);

  auto in_image = ImageType::New();
  in_image->SetRegions(region);
  in_image->SetNumberOfComponentsPerPixel(pix.GetSize());
  in_image->Allocate();
  in_image->SetPixel(start, pix);

  auto writer = otb::ImageFileWriter<ImageType>::New();
  writer->SetInput(in_image);
  writer->SetFileName(ima_name);
  writer->Update();
}
std::tuple<std::string, std::string, std::string, std::string, std::string> create_data_files(std::string path)
{
 
  unsigned int nbDates = 0;
  unsigned int nbOutputDates = 0;
  auto date_file = path+"/dates.txt";
  std::ofstream df(date_file);
  df << "20140101\n"; nbDates++;
  df << "20140106\n"; nbDates++;
  df << "20140112\n"; nbDates++;
  df << "20140115\n"; nbDates++;
  df.close();
  auto out_date_file = path+"/out_dates.txt";
  std::ofstream odf(out_date_file);
  odf << "20140101\n"; nbOutputDates++;
  odf << "20140106\n"; nbOutputDates++;
  odf << "20140109\n"; nbOutputDates++;
  odf << "20140112\n"; nbOutputDates++;
  odf << "20140115\n"; nbOutputDates++;
  odf.close();
  PixelType pix{nbDates};
  for(auto i=0; i<nbDates; i++)
    pix[i] = i;

  pix[2] = 20;
  auto in_image_file = path+"/in_image.tif";
  create_image(in_image_file,pix);

  auto mask_file = path+"/mask.tif";
  pix.Fill(0);
  pix[2] = 1;
  create_image(mask_file,pix);

  auto out_image_file = path+"/out_image.tif";
  return std::make_tuple(in_image_file, mask_file, out_image_file, date_file, out_date_file);
}

int imageFunctionGapfillingTest(int argc, char * argv[])
{
  if(argc!=2)
    {
    std::cout << argc-1 << " arguments given" << std::endl;
    std::cout << "Usage: " << argv[0] << " file_path " << std::endl;
    return EXIT_FAILURE;
    }
  std::string in_image_file, mask_file, out_image_file, date_file, out_date_file;
  std::tie(in_image_file, mask_file, out_image_file, date_file, out_date_file) = 
    create_data_files(argv[1]);
  size_t cpd = 1;
  GapFilling::gapfill_time_series<ImageType, LinearFunctorType>(in_image_file,
                                                                mask_file,
                                                                out_image_file,
                                                                cpd,
                                                                date_file);

  GapFilling::gapfill_time_series<ImageType, LinearFunctorType>(in_image_file,
                                                                mask_file,
                                                                out_image_file,
                                                                cpd,
                                                                date_file,
                                                                out_date_file);
    return EXIT_SUCCESS;
}
