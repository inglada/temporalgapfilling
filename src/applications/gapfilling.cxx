/*=========================================================================
  Program:   gapfilling
  Language:  C++

  Copyright (c) CESBIO. All rights reserved.

  See gapfilling-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include <cstdlib>
#include "gapfilling.h"

using ImageType = otb::VectorImage<float, 2>;
using LinearFunctorType =
  GapFilling::LinearGapFillingFunctor<ImageType::PixelType>;
using SplineFunctorType =
  GapFilling::SplineGapFillingFunctor<ImageType::PixelType>;

int main(int argc, char** argv)
{
  if(argc<6 || argc>7)
    {
    std::cerr << "Usage: " << argv[0] << " ndvi_series mask_series output_series "
              << "components_per_date interpolation_type (0: linear, 1: spline)"
              << " [date_file]"
              << std::endl;
    std::cerr << "Example: " << argv[0]
              << " /home/inglada/stok/SudOuest2008/TestsInterpolation/ndvi.hdr "
              << "/home/inglada/stok/SudOuest2008/TestsInterpolation/masks.hdr "
              << "/home/inglada/stok/SudOuest2008/TestsInterpolation/test.tif 1 1"
              << std::endl;
    return EXIT_FAILURE;
    }
  auto cpd = std::atoi(argv[4]);
  std::string date_file{""};
  if(argc==7)
    date_file = argv[6];
  if(!std::atoi(argv[5]))
    {
    std::cout << "Using linear interpolation and " << cpd
              << " components per date " << std::endl;
    GapFilling::gapfill_time_series<ImageType, LinearFunctorType>(argv[1],
                                                                  argv[2],
                                                                  argv[3],
                                                                  cpd,
                                                                  date_file);
    }
  else
    {
    std::cout << "Using spline interpolation and " << cpd
              << " components per date " << std::endl;
    GapFilling::gapfill_time_series<ImageType, SplineFunctorType>(argv[1],
                                                                  argv[2],
                                                                  argv[3],
                                                                  cpd,
                                                                  date_file);
    }
    return EXIT_SUCCESS;
}
