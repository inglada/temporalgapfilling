/*=========================================================================

  Program:   gapfilling
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See gapfilling-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef _GAPFILLING_H_
#define _GAPFILLING_H_

#include "otbVectorImage.h"
#include "otbImageFileReader.h"
#include "otbImageFileWriter.h"
#include "otbStandardFilterWatcher.h"
#include "itkBinaryFunctorImageFilter.h"
#include <vector>
#include <tuple>
#include <stdexcept>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

#include "dateUtils.h"

namespace GapFilling
{
/** Return 2 vectors containing, for each date, the position of the
* last (resp next) valid date and a bool which is true if there are no
* valid dates */
template <typename PixelType>
inline
std::tuple<std::vector<typename PixelType::ValueType>,
           std::vector<typename PixelType::ValueType>, bool>
find_valid_bounds(const PixelType mask, int nbDates,
                  typename PixelType::ValueType valid_value)
{
  using ValueType = typename PixelType::ValueType;
  using VectorType = typename std::vector<ValueType>;

  VectorType l_valid(nbDates,0);
  VectorType n_valid(nbDates,0);
  int lv{-1};
  int nv{nbDates};
  for(auto i=0; i<nbDates; i++)
    {
    if(mask[i]==(valid_value)) lv=i;
    l_valid[i] = lv;
    auto j = nbDates-1-i;
    if(mask[j]==(valid_value)) nv=j;
    n_valid[j] = nv;
    }
  return std::make_tuple(l_valid, n_valid, (lv==-1 && nv==nbDates));
}
/**  Generate a new pixel and a corresponding mask after interlacing
*  input and output dates. The resulting pixel values for the dates
*  which were not available in the input have a nundefined value and
*  are masked.
*/
template <typename PixelType>
inline
std::tuple<PixelType, PixelType, PixelType >
create_tmp_data_interlace_dates(const PixelType pix, 
                                const PixelType mask, 
                                const PixelType idv, 
                                const PixelType odv,
                                typename PixelType::ValueType valid_value)
{
  if(idv == odv)
    return std::make_tuple(pix, mask, odv);

  unsigned int nbDates = idv.GetSize() + odv.GetSize();
  PixelType opix{nbDates};
  PixelType omask{nbDates};
  PixelType dv{nbDates};

  unsigned int dcount = 0;
  unsigned int icount = 0;
  unsigned int ocount = 0;

  while(dcount < nbDates)
    {
    if(icount < idv.GetSize() &&
       (ocount == odv.GetSize() || //ouput dates consumed
        idv[icount] <= odv[ocount]))
      {
      opix[dcount] = pix[icount];
      omask[dcount] = mask[icount];
      dv[dcount] = idv[icount];
      icount++;
      }
    else
      {
      opix[dcount] = typename PixelType::ValueType{0};
      omask[dcount] = valid_value+1;
      dv[dcount] = odv[ocount];
      ocount++;
      }
      dcount++;
    }
    return std::make_tuple(opix, omask, dv);
}
/*** Return a pixel with only the output dates. dv contains all dates
* and only those contained in odv are kept.
*/
template <typename PixelType>
inline
PixelType  extract_output_dates(const PixelType pix, 
                                const PixelType dv, 
                                const PixelType odv)
{
  if(dv == odv)
    return pix;

  unsigned int nbDates = odv.GetSize();
  unsigned int nbInDates = dv.GetSize();

  if(nbDates > nbInDates)
      throw std::invalid_argument("There are more output dates than input dates\n");
  PixelType result{nbDates};

  unsigned int in_count = 0;
  unsigned int out_count = 0;

  while(in_count < dv.GetSize() && out_count < nbDates)
    {
    if(dv[in_count] == odv[out_count])
      {
      result[out_count] = pix[in_count];
      ++out_count;
      }
    ++in_count;
    }

  return result;
}

template <typename PixelType>
class IdentityGapFillingFunctor
{
public:
  IdentityGapFillingFunctor() = default;
  IdentityGapFillingFunctor(const PixelType& d) : dv{d} {}

  PixelType operator()(PixelType pix, PixelType mask)
  {
    return pix;
  }

  bool operator!=(const IdentityGapFillingFunctor a)
  {
    return (this->dates != a.dates) || (this->dv != a.dv) ;
  }

  bool operator==(const IdentityGapFillingFunctor a)
  {
    return !(*this != a);
  }

protected:
  PixelType dv;

};

template <typename PixelType>
class LinearGapFillingFunctor
{
public:
  using ValueType = typename PixelType::ValueType;
  using VectorType = typename std::vector<ValueType>;
  ValueType valid_value = ValueType{0};
  ValueType invalid_pixel_return_value = ValueType{0};
  LinearGapFillingFunctor() = default;
  /// Constructor with a vector of input dates
  LinearGapFillingFunctor(const PixelType& d) : dv{d} {}
  /// Constructor with vectors of input and output dates
  LinearGapFillingFunctor(const PixelType& d, const PixelType& od) 
    : dv{d}, odv{od} {}

  // valid pixel has a mask==0
  PixelType operator()(PixelType pix, PixelType mask)
  {
    unsigned int nbDates = pix.GetSize();
    if(nbDates != mask.GetSize())
      throw std::invalid_argument("Pixel and mask have different sizes\n");
    if(dv.GetSize()!=0 && nbDates != dv.GetSize())
      throw
        std::invalid_argument("Pixel and date vector have different sizes\n");
    if(dv.GetSize()==0)
      {
      dv = pix;
      for(auto i=0; i<nbDates; i++)
        dv[i] = i;
      }
    if(odv.GetSize()==0) odv = dv;

    unsigned int nbOutputDates = odv.GetSize();
    PixelType validpix{nbDates};
    validpix.Fill(valid_value);
    PixelType invalidpix{nbOutputDates};
    invalidpix.Fill(invalid_pixel_return_value);
    // If the mask says all dates are valid and the input and output
    // dates are the same, keep the original value
    if(mask == validpix && dv == odv) return pix;
    // Interlace input and output dates
    PixelType tmp_pix, tmp_mask, tmp_dates;
    std::tie(tmp_pix, tmp_mask, tmp_dates) = 
      create_tmp_data_interlace_dates(pix, mask, dv, odv, valid_value);
    // For each component, find the position of the last and the next valid
    // values
    VectorType last_valid;
    VectorType next_valid;
    bool invalid_pixel;
    std::tie(last_valid, next_valid, invalid_pixel) =
      find_valid_bounds(tmp_mask, tmp_mask.GetSize(), valid_value);
    // invalid pixel?
    if(invalid_pixel)
      return invalidpix;

    return extract_output_dates(this->interpolate(tmp_pix, tmp_mask, 
                                                  tmp_dates, last_valid, 
                                                  next_valid), 
                                tmp_dates, odv);
  }

  bool operator!=(const LinearGapFillingFunctor a)
  {
    return (this->dv != a.dv) ;
  }

  bool operator==(const LinearGapFillingFunctor a)
  {
    return !(*this != a);
  }

protected:
  inline
  PixelType interpolate(const PixelType& p, const PixelType& m, const PixelType& d,
                        const VectorType& lv, const VectorType& nv)
  {
    unsigned int nbDates = p.GetSize();
    PixelType result(nbDates);
    for(auto i=0; i<nbDates; i++)
      {
      auto lvp = lv[i];
      auto nvp = nv[i];
      if(m[i]==(valid_value))
        result[i] = p[i];
      else
        {
        // If there is no previous valid value, just use the next one
        if(lvp==-1)
          result[i] = p[nvp];
        // If there is no next valid value, just use the last one
        else if(nvp==nbDates)
          result[i] = p[lvp];
        // Otherwise, use linear interpolation
        else
          {
          double x1 = d[lvp];
          double y1 = p[lvp];
          double x2 = d[nvp];
          double y2 = p[nvp];
          double a = (y2-y1)/(x2-x1);
          double b = ((y1+y2)*(x2-x1)-(y2-y1)*(x2+x1))/(2*(x2-x1));

          result[i] = a*d[i]+b;
          }
        }
      }
    return result;
  }
  
  /// Input date vector
  PixelType dv;
  /// Output date vector
  PixelType odv;

};

template <typename PixelType>
class SplineGapFillingFunctor
{
public:
  using ValueType = typename PixelType::ValueType;
  using VectorType = typename std::vector<ValueType>;
  ValueType valid_value = ValueType{0};
  ValueType invalid_pixel_return_value = ValueType{0};
  SplineGapFillingFunctor() = default;

  SplineGapFillingFunctor(const PixelType& d) : dv{d} {}

  /// Constructor with vectors of input and output dates
  SplineGapFillingFunctor(const PixelType& d, const PixelType& od) 
    : dv{d}, odv{od} {}


  // valid pixel has a mask==0
  PixelType operator()(PixelType pix, PixelType mask)
  {
    unsigned int nbDates = pix.GetSize();
    bool no_dates{dv.GetSize() == 0};
    if(nbDates != mask.GetSize())
      throw std::invalid_argument("Pixel and mask have different sizes\n");
    if(!no_dates && nbDates != dv.GetSize())
      throw
        std::invalid_argument("Pixel and date vector have different sizes\n");
    PixelType validpix{nbDates};
    validpix.Fill(valid_value);
    PixelType invalidpix{nbDates};
    invalidpix.Fill(invalid_pixel_return_value);
    // If the mask says all dates are valid, keep the original value
    if(mask == validpix) return pix;
    //Otherwise, interpolate
    VectorType last_valid;
    VectorType next_valid;
    bool invalid_pixel;
    std::tie(last_valid, next_valid, invalid_pixel) =
      find_valid_bounds(mask, nbDates, valid_value);
    // invalid pixel?
    if(invalid_pixel)
      return invalidpix;
    return this->interpolate(pix, mask, last_valid, next_valid, no_dates);
  }

  bool operator!=(const SplineGapFillingFunctor a)
  {
    return (this->dv != a.dv) ;
  }

  bool operator==(const SplineGapFillingFunctor a)
  {
    return !(*this != a);
  }

protected:
    inline
    PixelType interpolate(const PixelType& p, const PixelType& m,
                          const VectorType& lv, const VectorType& nv,
                          bool no_dates)
  {
    unsigned int nbDates = p.GetSize();
    // Prepare the data for gsl
    double* x = new double[nbDates];
    double* y = new double[nbDates];
    std::size_t nbValidDates{0};
    for(auto i = 0; i < nbDates; i++)
    {
    if(m[i]==(valid_value))
      {
      x[nbValidDates] = (no_dates)?(static_cast<double>(i)):dv[i];
      y[nbValidDates] = p[i];
      nbValidDates++;
      }
    }
    gsl_interp_accel* acc = gsl_interp_accel_alloc();
    gsl_spline* spline;
    switch(nbValidDates)
      {
      case 0:
      case 1:
        return p;
      case 2:
        spline = gsl_spline_alloc(gsl_interp_linear, nbValidDates);
        break;
      case 3:
      case 4:
        spline = gsl_spline_alloc(gsl_interp_cspline, nbValidDates);
        break;
      default:
        spline = gsl_spline_alloc(gsl_interp_akima, nbValidDates);
      }

    gsl_spline_init(spline, x, y, nbValidDates);
    // the real interpolation
    PixelType result(nbDates);
    for(auto i=0; i<nbDates; i++)
      {
      auto lvp = lv[i];
      auto nvp = nv[i];
      if(m[i]==(valid_value))
        result[i] = p[i];
      else
        {
        // If there is no previous valid value, just use the next one
        if(lvp==-1)
          result[i] = p[nvp];
        // If there is no next valid value, just use the last one
        else if(nvp==nbDates)
          result[i] = p[lvp];
        // Otherwise, use spline interpolation
        else
          {
          auto date = (no_dates)?(static_cast<double>(i)):dv[i];
          result[i] = gsl_spline_eval(spline, date, acc);
          }
        }
      }
    gsl_spline_free(spline);
    gsl_interp_accel_free(acc);
    delete [] x;
    delete [] y;
    return result;
  }
  /// Input date vector
  PixelType dv;
  /// Output date vector
  PixelType odv;
};

/**
Adapts a functor operating on time series so that it can work with
series which have several components per date. The p2 pixel can have just one component per date.
*/
template <typename PixelType, typename FunctorType>
struct MultiComponentTimeSeriesFunctorAdaptor {
  MultiComponentTimeSeriesFunctorAdaptor() : m_NumberOfComponentsPerDate(1){};
  PixelType operator()(PixelType p1, PixelType p2)
  {
    auto nbComponents = p1.GetSize();
    auto nbDates = nbComponents/m_NumberOfComponentsPerDate;
    if(p1.GetSize()!=p2.GetSize() && p2.GetSize()!=nbDates)
      throw
        std::invalid_argument("p2 has to have either the same size as p1 or one component per date\n");
    auto result = PixelType(nbComponents);
    for(auto band=0; band<m_NumberOfComponentsPerDate; band++)
      {
      auto tmp1 = PixelType(nbDates);
      auto tmp2 = PixelType(nbDates);
      for(auto date=0; date<nbDates; date++)
        tmp1[date] = p1[band+date*m_NumberOfComponentsPerDate];

      PixelType tmp_res;
      // If p1 and p2 have the same sizes, demux also p2
      if(p1.GetSize() == p2.GetSize())
        {
        for(auto date=0; date<nbDates; date++)
          tmp2[date] = p2[band+date*m_NumberOfComponentsPerDate];
        tmp_res = m_Functor(tmp1, tmp2);
        }
      // Otherwise, use the same p2 for all components of p1
      else
        tmp_res = m_Functor(tmp1, p2);
      for(auto date=0; date<nbDates; date++)
        result[band+date*m_NumberOfComponentsPerDate] = tmp_res[date];
      }
    return result;    
  }
  void SetNumberOfComponentsPerDate(size_t n)
  {
    m_NumberOfComponentsPerDate = n;
  }
  void SetFunctor(FunctorType f)
  {
    m_Functor = f;
  }
  FunctorType* GetFunctor()
  {
    return &m_Functor;
  }
private:
  size_t m_NumberOfComponentsPerDate;
  FunctorType m_Functor;
};

/**
The gapfill_time_series function takes 2 input time series files (the
image data and the mask data) and produces an output file (the
gapfilled image data). The number of components (spectral bands, for
instance) per date can be provided (default is 1) so that the
different components are processed as individual time series. A file
containing the dates of the acquisition can be provided. It should
contain one date per line in YYYMMDD format.

The mask time series is supposed to contain 0s for the valid dates.
The function is templated over the ImageType (typically an
otb::VectorImage<double,2>) and on the interpolating functor.
*/
template <typename ImageType, typename FunctorType>
void gapfill_time_series(std::string ima_name, std::string mask_name, 
                         std::string out_name, 
                         size_t components_per_date = 1, 
                         std::string date_file="",
                         std::string out_date_file="")
{
  auto readerIma = otb::ImageFileReader<ImageType>::New();
  auto readerMask = otb::ImageFileReader<ImageType>::New();
  readerIma->SetFileName(ima_name);
  readerMask->SetFileName(mask_name);

  using PixelType = typename ImageType::PixelType;
  using ValueType = typename PixelType::ValueType;
  PixelType dv, odv;
  if(date_file != "")
    {
    std::cout << "Using date file " << date_file << std::endl;
    auto date_vec = pheno::parse_date_file(date_file);
    std::vector<ValueType> doy_vector(date_vec.size(), ValueType{0});
    std::transform(std::begin(date_vec), std::end(date_vec),
                   std::begin(doy_vector), pheno::doy);
    dv = PixelType(doy_vector.data(), doy_vector.size());
    }
  if(out_date_file != "")
    {
    std::cout << "Using output date file " << out_date_file << std::endl;
    auto date_vec = pheno::parse_date_file(out_date_file);
    std::vector<ValueType> doy_vector(date_vec.size(), ValueType{0});
    std::transform(std::begin(date_vec), std::end(date_vec),
                   std::begin(doy_vector), pheno::doy);
    odv = PixelType(doy_vector.data(), doy_vector.size());
    }
  using MultiComponentFunctorType =
    MultiComponentTimeSeriesFunctorAdaptor<typename ImageType::PixelType,
                                           FunctorType>;
  auto filter = itk::BinaryFunctorImageFilter<ImageType, ImageType, ImageType,
                                              FunctorType>::New();
  auto filter_mc =
    itk::BinaryFunctorImageFilter<ImageType, ImageType, ImageType,
                                  MultiComponentFunctorType>::New();
  readerIma->GetOutput()->UpdateOutputInformation();
  readerMask->GetOutput()->UpdateOutputInformation();
  auto writer = otb::ImageFileWriter<ImageType>::New();
  if(components_per_date==1)
    {
    if(out_date_file != "")
      filter->SetFunctor(FunctorType(dv, odv));
    else if(date_file != "")
      filter->SetFunctor(FunctorType(dv));
    filter->SetInput(0, readerIma->GetOutput());
    filter->SetInput(1, readerMask->GetOutput());
    writer->SetInput(filter->GetOutput());
    }
  else
    {
    if(out_date_file != "")
      (filter_mc->GetFunctor()).SetFunctor(FunctorType(dv, odv));
    else if(date_file != "")
      (filter_mc->GetFunctor()).SetFunctor(FunctorType(dv));
    (filter_mc->GetFunctor()).SetNumberOfComponentsPerDate(components_per_date);
    filter_mc->SetInput(0, readerIma->GetOutput());
    filter_mc->SetInput(1, readerMask->GetOutput());
    writer->SetInput(filter_mc->GetOutput());
    }
  writer->SetFileName(out_name);
  otb::StandardFilterWatcher watcher(writer, "Gapfilling");
  writer->Update();
}

}//GapFilling namespace

#endif
